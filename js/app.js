(function ($) {

	var _app = {

		// App Profile Expand

		profile_expand : function() {
			$('.btn-expand').click(function() {
				$(this).toggleClass("btn__round--left");
				

				if ($(window).width() < 960) {
				   $('.profile__expand').toggle("slide", {direction:"up"}, 800);
				   $("html, body").animate({ scrollTop: $(document).height() }, 900);
				} else {
					$('.profile__expand').toggle("slide", {direction:"left"}, 500);
				}
			})
		},

		date_picker : function() {
			$('#dob').datepicker();
		},

		validate : function() {
			   $.validate({
			   	errorMessagePosition : "inline"
			   });
		},
				        

		
		// INIT //
		init : function() {
			_app.profile_expand();
			_app.date_picker();
			_app.validate();			
		}

	// Function End

};


$(function() {
	_app.init();
});


})(jQuery);
